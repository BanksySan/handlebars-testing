# HandlebarsJS  with external views#

HandlebarsJS does not support external views, this code demonstrates how it can be achieved.

### What is this repository for? ###

* Version 0.0.1

### How do I get set up? ###

Run with `node ./server.js`.  Server is set up on `localhost:8888` by default.

### Contribution guidelines ###

* All JavaScript code must be linted and places in subdirectory of `app/js`.  Lint code with `grunt jshint`.

### Who do I talk to? ###

* David Banks (me)