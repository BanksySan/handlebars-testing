var handlebarsTest = handlebarsTest || {};

(function(ns, $) {
    ns.init = function(success) {
        var compileViews, ajaxPromises = [];

        compileViews = function (success) {
            var viewsFolder = 'app/views/';

            ns.viewsPaths = [
                {name: 'view-1', path: viewsFolder + 'view-1.view'},
                {name: 'view-2', path: viewsFolder + 'view-2.view'}
            ];

            ns.views = [];
            console.log(JSON.stringify(ns, null, 4));
            ns.viewsPaths.forEach(function (item) {
                ajaxPromises.push($.ajax({
                    url: item.path,
                    success: function (data) {
                        console.log(JSON.stringify(data, null, 4));
                        var compiledTemplate = Handlebars.compile(data);
                        ns.views.push({name: item.name, template: compiledTemplate});
                    },
                    error: function (error) {
                        console.error(JSON.stringify(error, null, 4));
                    }
                }));
            });

            $.when(ajaxPromises).then(success);
        };

        compileViews(success);
    };
})(handlebarsTest, $);