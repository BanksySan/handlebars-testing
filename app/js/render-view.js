(function(ns)
{
    ns.renderView = function(viewName, data, target)
    {
        var compiledTemplate = $.grep(ns.views, function(item)
        {
            return item.name === viewName;
        });

        if (compiledTemplate.length !== 1) {
            throw "Expected to find on view with name '" + viewName + "', but found " + compiledTemplate.length + '.';
        } else {
            target.html(compiledTemplate[0].template(data));
        }
    };
})(handlebarsTest);